import com.rivigo.payments.payU.PayuHttp;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Main {
  public static void main(String[] args) {
    try {
      System.out.println(PayuHttp.makePayment("ABHI003", 1, "SBIB"));
      System.out.println(PayuHttp.makePayment(Long.toHexString(System.nanoTime()), 1, "SBIB"));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }
}
