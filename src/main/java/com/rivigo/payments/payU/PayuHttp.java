package com.rivigo.payments.payU;

import lombok.experimental.UtilityClass;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@UtilityClass
public class PayuHttp {
  private static OkHttpClient client;

  private static Request.Builder paymentRequestBuilder;
  private static final String SALT = "uVWVYwnM";
  private static final String KEY = "aF5bUt";
  private static final String firstname = "Vyom";
  private static final String productinfo = "Demand_wallet";
  private static final String email = "payments@rivigo.com";

  static {
    try {
      initSslClient();
      paymentRequestBuilder =
          new Request.Builder()
              .url("https://secure.payu.in/_payment")
              .addHeader("cache-control", "no-cache,no-cache")
              .addHeader("Accept", "application/json")
              .addHeader("content-type", "application/x-www-form-urlencoded");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void initSslClient()
      throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException, IOException,
          CertificateException, UnrecoverableKeyException {

    TrustManagerFactory trustManagerFactory =
        TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    trustManagerFactory.init((KeyStore) null);
    TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
    if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
      throw new IllegalStateException(
          "Unexpected default trust managers:" + Arrays.toString(trustManagers));
    }
    X509TrustManager trustManager = (X509TrustManager) trustManagers[0];

    KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType()); // "JKS"
    ks.load(null, null);
    KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    kmf.init(ks, null);
    KeyManager[] kms = kmf.getKeyManagers();

    SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
    sslContext.init(kms, null, null);
    SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
    client =
        new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .sslSocketFactory(sslSocketFactory, trustManager)
            .build();
  }

  private static String generateHash(String transactionId, Float amount)
      throws NoSuchAlgorithmException {
    String text =
        String.format(
            "%s|%s|%s|%s|%s|%s|||||||||||%s",
            KEY, transactionId, amount.toString(), productinfo, firstname, email, SALT);

    MessageDigest md = MessageDigest.getInstance("SHA-512");
    byte[] messageDigest = md.digest(text.getBytes(StandardCharsets.UTF_8));
    // Convert byte array into signum representation
    BigInteger no = new BigInteger(1, messageDigest);
    // Convert message digest into hex value
    String hashtext = no.toString(16);
    // Add preceding 0s to make it 32 bit
    while (hashtext.length() < 32) {
      hashtext = "0" + hashtext;
    }
    return hashtext;
  }

  private static RequestBody paymentBody(String transactionId, Float amount, String bankCode)
      throws NoSuchAlgorithmException {
    return new FormBody.Builder()
        .add("key", KEY)
        .add("txnid", transactionId)
        .add("amount", amount.toString())
        .add("productinfo", productinfo)
        .add("firstname", firstname)
        .add("email", email)
        .add("phone", "9999999999")
        .add("txn_s2s_flow", "1")
        .add("pg", "NB")
        .add("bankcode", bankCode)
        .add("surl", "http://demand-dev.vyom.com/8080/api/user/fund/add/status/update")
        .add("furl", "http://demand-dev.vyom.com/8080/api/user/fund/add/status/update")
        .add("hash", generateHash(transactionId, amount))
        .add("s2s_client_ip", "172.12.34.23")
        .add("s2s_device_info", "mobile")
        .build();
  }

  public static Optional<String> makePayment(String transactionId, int amount, String bankCode)
      throws IOException, NoSuchAlgorithmException {
    Request request =
        paymentRequestBuilder
            .post(paymentBody(transactionId, Float.valueOf(amount), bankCode))
            .build();
    Response response = client.newCall(request).execute();
    if (response.code() == 200 && response.body() != null) {
      return Optional.ofNullable(response.body().string());
    }
    return Optional.empty();
  }
}
